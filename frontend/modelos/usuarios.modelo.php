<?php

require_once "conexion.php";

class ModeloUsuarios{

/*===================================== 
        REGISTRO DE USUARIO
======================================*/

static public function mdlRegistroUsuario($tabla, $datos) {
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre, email, password, modo, foto, verificacion, email_encriptado)
     VALUES(:nombre, :email, :password,  :modo, :foto, :verificacion, :email_encriptado)");
    
    $stmt->bindParam(":nombre",$datos["nombre"], PDO::PARAM_STR);
    $stmt->bindParam(":email",$datos["email"], PDO::PARAM_STR);
    $stmt->bindParam(":password",$datos["password"], PDO::PARAM_STR);
    $stmt->bindParam(":modo",$datos["modo"], PDO::PARAM_STR);
    $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
    $stmt->bindParam(":verificacion",$datos["verificacion"], PDO::PARAM_INT);
    $stmt->bindParam(":email_encriptado",$datos["email_encriptado"], PDO::PARAM_STR);


        if ($stmt-> execute()) {
            return "ok";
        } else {
            return "error";
        }

        $stmt->close();
        $stmt= null;

        }


/*===================================== 
        MOSTRAR USUARIO
======================================*/
static public function mdlMostrarUsuario($tabla, $item, $valor){
    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
    $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
    $stmt -> execute();
    return $stmt -> fetch();
    $stmt-> close();
    $stmt = null;
}


/*===================================== 
        ACTUALIZAR USUARIO
======================================*/
static public function mdlActualizarUsuario($tabla, $id, $item, $valor){
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item= :$item  WHERE $id = :id");
    $stmt->bindParam(":".$item,$valor, PDO::PARAM_STR);
    $stmt->bindParam(":id",$id, PDO::PARAM_INT);

    if($stmt->execute()){
        return "ok";
    } else {
        return "error";
    }

    $stmt->close();
    $stmt = null;
}


/*===================================== 
        ACTUALIZAR PERFIL
======================================*/
static public function mdlActualizarPerfilUsuario($tabla, $datos){
    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, email = :email, password = :password, foto = :foto  WHERE id = :id");
    $stmt->bindParam(":nombre",$datos["nombre"], PDO::PARAM_STR);
    $stmt->bindParam(":email",$datos["email"], PDO::PARAM_STR);
    $stmt->bindParam(":password",$datos["password"], PDO::PARAM_STR);
    $stmt->bindParam(":foto",$datos["foto"], PDO::PARAM_STR);
    $stmt->bindParam(":id",$datos["id"], PDO::PARAM_INT);

    if($stmt->execute()){
        return "ok";
    } else {
        return "error";
    }

    $stmt->close();
    $stmt = null;
}

/*===================================== 
        MOSTRAR COMPRAS
======================================*/
static public function mdlMostrarCompras($tabla, $item, $valor){
    $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
    $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
    $stmt -> execute();
    return $stmt -> fetchAll();
    $stmt-> close();
    $stmt = null;
}

/*===========================================================
            MUESTRA LOS COMENTARIOS DE LOS USUARIOS
============================================================*/
    static public function mdlMostarComentariosPerfil($tabla,$datos){

        if ($datos["idUsuario"] != "") {

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto ");
            $stmt -> bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
            $stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
            $stmt -> execute();
            return $stmt -> fetch();
        
        } else {

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_producto = :id_producto ORDER BY Rand()");
            $stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
            $stmt -> execute();
            return $stmt -> fetchAll();

        }

        
        $stmt -> close();
        $stmt = null;    
    }

    /*=====================================================
            ACTUALIZAR COMENTARIO DE USUARIO
    =======================================================*/
    static public function mdlActualizarComentario($tabla, $datos){

        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET calificacion = :calificacion, comentario = :comentario WHERE id = :id");

        $stmt->bindParam(":calificacion", $datos["calificacion"], PDO::PARAM_STR);
        $stmt->bindParam(":comentario", $datos["comentario"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

        if($stmt -> execute()){

            return "ok";

        } else {

            return "error";
        }

        $stmt->close();
        $stmt = null;

    }



        }