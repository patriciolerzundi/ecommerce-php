/*===============================
    CARRUSEL
===============================*/
$(".flexslider").flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5
});

$(".flexslider ul li img").click(function () {

    var capturaIndice = $(this).attr("value");
    $(".infoproducto figure.visor img").hide();
    $("#lupa" + capturaIndice).show();

});
/*===============================
    EFECTO LUPA
===============================*/
$(".infoproducto figure.visor img").mouseover(function (event) {

    var capturaImg = $(this).attr("src");
    $(".lupa img").attr("src", capturaImg);
    $(".lupa").fadeIn("fast");
    $(".lupa").css({
        "height": $(".visorImg").height() + "px",
        "background": "#eee",
        "width": "100%"
    });
});

$(".infoproducto figure.visor img").mouseout(function (event) {
    $(".lupa").fadeOut("fast");
});


$(".infoproducto figure.visor img").mousemove(function (event) {
    var posX = event.offsetX;
    var posY = event.offsetY;
    $(".lupa img").css({
        "margin-left": -posX + "px",
        "margin-top": -posY + "px"

    });
});
/*===============================
    CONTADOR DE VISTAS
===============================*/

var contandor = 0;

$(window).on("load", function () {
    var vistas = $("span.vistas").html();
    var precio = $("span.vistas").attr('tipo');
    contandor = Number(vistas) + 1;
    $("span.vistas").html(contandor);

    /*  EVALUAMOS PRECIO PARA DEFINIR CAMPO A ACTUALIZAR */

    if (precio == 0) {

        var item = "vistas_gratis";

    } else {
        var item = "vistas";
    }

    /*  EVALUAMOS RUTAS PARA DEFINIR EL PRODUCTO A ACTUALZIAR */
    var url_actual = location.pathname;
    var ruta = url_actual.split("/");
    var datos = new FormData();

    datos.append("valor", contandor);
    datos.append("item", item);
    datos.append("ruta",ruta.pop())
    
    $.ajax({

		url:rutaOculta+"ajax/producto.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData:false,
		success: function(respuesta){
        }

	});

});

/*  ALTURA COMENTARIOS */
$(".comentatios").css({"height":$(".comentarios .alturaComentarios").height()+"px",
                        "overflow":"hidden",
                        "margin-bottom":"20px"});

$("#verMas").click(function(e) {
    e.preventDefault();
    if ($("#verMas").html() == "Ver Más") {
        $(".comentatios").css({"overflow":"inherit"});
        $("#verMas").html("Ver Menos")
    }

    else {
        $(".comentatios").css({"height":$(".comentarios .alturaComentarios").height()+"px",
                        "overflow":"hidden",
                        "margin-bottom":"20px"});
        $("#verMas").html("Ver Más")
    }


});